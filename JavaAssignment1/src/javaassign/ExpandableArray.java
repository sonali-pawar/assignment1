/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaassign;

import java.util.ArrayList;
import java.util.Collections;
import static java.util.Collections.list;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 *
 * @author root
 */
public class ExpandableArray {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
     
        List<Integer> expandableList = new ArrayList<>();

        // take input from user
        System.out.println("Enter elements here= ");
        Scanner scanIn = new Scanner(System.in);
        Scanner input = new Scanner(scanIn.nextLine());

        while (input.hasNext()) {
            if (input.hasNextInt()) { // if integers
                int count = input.nextInt();
                if (count >= 0) {
                    expandableList.add(count); // add tp arraylist
                    
                    //remove duplicate no from arraylist
                    List<Integer> nonDulicateList = expandableList.stream().distinct().collect(Collectors.toList());
                    System.out.println("size of nonDulicateList = " + nonDulicateList);
                    System.out.println("size of expandableList = " + nonDulicateList.size());

                }
            } else { // strings

                System.out.println("please enter integer... ");
                input = new Scanner(scanIn.nextLine());
                expandableList.clear();

            }
        }

        scanIn.close();
        
        // use MVC pattern
        Model model = getMaxMinValue(expandableList);
        ExpandableArrayView view = new ExpandableArrayView();
        ExpandableArrayController controller = new ExpandableArrayController(model, view);
        controller.updateView();

    }


    private static Model getMaxMinValue(List<Integer> expandableList) {
        Model model = new Model();
        try {
            if (!expandableList.isEmpty()) {
                // using collections class find out min & max max number
                int max = Collections.max(expandableList);
                int min = Collections.min(expandableList);
                model.setMin(min);
                model.setMax(max);
                System.out.println("min= " + min + " max= " + max);
            }
        } catch (NullPointerException e) {
            System.out.println("exception" + e.getMessage());

        } catch (ClassCastException e) {
            System.out.println("exception" + e.getMessage());

        } catch (ArithmeticException e) {
            System.out.println("exception" + e.getMessage());

        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("exception" + e.getMessage());
        }
        //        catch(Exception e){
//         System.out.println("exception"+e.getMessage());
// 
//        }
        return model;
    }

}
