/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaassign;

/**
 *
 * @author root
 */
public class ExpandableArrayController {
    private Model model;
    private ExpandableArrayView view;

    public ExpandableArrayController(Model model, ExpandableArrayView view) {
        this.model = model;
        this.view = view;
    }
    
    
    
    
     public void setMinimum(int minimum){
      model.setMin(minimum);		
   }

   public int getMinimum(){
      return model.getMin();
   }

   public void setMaximum(int maximum){
      model.setMax(maximum);		
   }

   public int getMaximum(){
      return model.getMax();		
   }

   public void updateView(){				
      view.printarrayDetails(model.getMin(), model.getMax());
   }	
}
